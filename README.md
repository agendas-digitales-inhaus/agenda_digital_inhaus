
# Agendas Digitales InHaus

Sistema de agendación y administración de citas para InHaus


## Desarrolladores

- Rodrigo Andrey Araya Castro
- Andrés Esteban Mora García
- Mario Enrique Rodriguez Loria
- Ernesto Andrés Hernández Naranjo





## Requisitos de instalación

- PHP 8.0+
- Laravel 8
- MySql
- Apache Server

## Instalación

Para instalar el proyecto localmente debe de poseer todos los elementos listados en los requisitos, posteriormente en su ambiente MySql debe crear una nueva base de datos llamada "AgendasDigitales", luego dentro de esta importar el archivo llamado "AgendasDigitales.sql" 
cambiar las referencias del archivo ".env" por sus propias
```bash
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
Con este ambiente preparado proceda con los siguientes comandos en ese orden

```bash
  $ composer install

  $ git clone https://{{Usuario}}@bitbucket.org/agendas-digitales-inhaus/agenda_digital_inhaus.git

  $ php artisan serve

```
Posteriormente visitar el enlace http://localhost:8000/ para visualizar el proyecto

## Modulos

- Autenticación
- Citas
- Usuarios
- Sedes


## Screenshots

![Dashboard](https://ibb.co/c8FM0fg)
![Usuarios](https://ibb.co/rZh0z9d)
![Sedes](https://ibb.co/H2BbBcN)


## Tech Stack

**Client:** Laravel, Bootstrap

**Server:** MySql, Apache


## 🔗 Enlaces
- [Sistema InHaus](http://34.83.130.126/)
- [Repositorio](https://bitbucket.org/agendas-digitales-inhaus/agenda_digital_inhaus/src/master/)


