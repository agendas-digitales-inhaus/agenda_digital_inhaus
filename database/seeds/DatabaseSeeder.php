<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder

{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'apellido1' => '1',
            'apellido2' => '2',
            'email' => 'a@admin.com',
            'password' => '$2y$10$fg4fpsmeWwgw63FIAcZs0uVd.jhhwPHO3hb47d9Qw0ZEEG.5qu23S', //admin123
            'cedula' => '123456',
            'role' => '2',
            'sede' => 'cartago',

        ]);
    }
}
