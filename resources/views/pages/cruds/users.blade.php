@extends('layout.master')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
@endpush


@section('content')
   <!-- <div class="col-12 col-xl-12 stretch-card">-->
        <!--<div class="row flex-grow-1">-->
        <div class="row">
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                            <h6 class="card-title mb-0">cantidad de asesores</h6>

                        </div>
                        <div class="row">
                            <div class="col-6 col-md-12 col-xl-5">
                                <h3 class="mb-2">{{ count($prueba) }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                            <h6 class="card-title mb-0"> Cantidad de jefes de oficina</h6>

                        </div>
                        <div class="row">
                        <div class="col-6 col-md-12 col-xl-5">
                                <h3 class="mb-2">{{ count($jefe) }}</h3>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                            <h6 class="card-title mb-0"> Cantidad de Administradores</h6>

                        </div>
                        <div class="row">
                        <div class="col-6 col-md-12 col-xl-5">
                                <h3 class="mb-2">{{ count($admin) }}</h3>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
            <br>


        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Añadir usuario</h6>
                        <form action="{{ route('usersStore') }}" method="POST">
                            @csrf
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="mb-3">
                                       <label for="IdAsesor">Cédula</label>
                                    <input type="number" name="cedula" class="form-control mr-4" id="IdAsesor"
                                        autocomplete="off" placeholder="11569874" required>
                                    </div>
                                  </div>
    
                                  <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="NombreAsesor">Nombre</label>
                                        <input name="name" type="text" class="form-control" id="NombreAsesor"
                                            placeholder="Ricardo" required>
                                    </div>
                                  </div>

                                  <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="ApellidoAsesor">Primer apellido</label>
                                    <input type="text" name="apellido1" class="form-control" id="ApellidoAsesor"
                                        autocomplete="off" placeholder="Salas" required>
                                    </div>
                                  </div>

                                  <div class="col-sm-6">
                                    <div class="mb-3">
                                    <label for="ApellidoAsesor">Segundo apellido</label>
                                    <input type="text" name="apellido2" class="form-control" id="ApellidoAsesor"
                                        autocomplete="off" placeholder="Muñoz" required>
                                    </div>
                                  </div>

                                  <div class="col-sm-6">
                                    <div class="mb-3">
                                        <label for="CorreoAsesor">Correo</label>
                                        <input name="email" type="email" class="form-control " id="CorreoAsesor" autocomplete="off"
                                            placeholder="RicardoSalas@gmail.com" required>
                                  </div>

                               
                                    <!--<div>
                                        <label class="form-label">Sedes</label>
                                        <select class="form-select from-select-sm" role="group" id="radioGroup">
                                            <option value="heredia" id="SedeHeredia">Heredia</option>
                                            <option value="sanjose" id="SedeSanJose">San José</option>
                                            <option value="cartago" id="SedeCartago">Cartago</option>
                                        </select>
                                    </div>-->
        
    
                                    <label class="form-label">Sedes</label><br>
                                  <!-- <select class="form-select" name="sede" role="group" id="radioGroup"
                                        aria-label="Basic radio toggle button group" required>
    
                                        <input type="radio" class="btn-check" name="sede" id="SedeHeredia" value="heredia"
                                            autocomplete="off">
                                        <label for="SedeHeredia" class="btn btn-outline-primary">Heredia</label>
    
                                        <input type="radio" class="btn-check" name="sede" id="SedeSanJose" value="sanjose"
                                            autocomplete="off">
                                        <label for="SedeSanJose" class="btn btn-outline-primary">San José</label>
    
                                        <input type="radio" class="btn-check" name="sede" id="SedeCartago" value="cartago"
                                            autocomplete="off">
                                        <label for="SedeCartago" class="btn btn-outline-primary">Cartago</label>
                                    </select> -->
                                    <select class="form-select" name="sede" aria-label=".form-select"
                                     required>
    
                                        <option value="San José Norte">San José Norte</option>
                                        <option value="San José Sur">San José Sur</option>
                                        <option value="San José Este">San José Este</option>
                                        <option value="San José Oeste">San José Oeste</option>
                                        <option value="Cartago">Cartago</option>
                                        <option value="Heredia">Heredia</option>
                                        <option value="Comercial">Comercial</option>
    
                        
                                    </select>
                                </div>
                             
    
    
                                <!--<div>
                                    <label class="form-label">Roles</label>
                                    <select class="form-select from-select-sm" role="group" id="roleGroup">
                                        <option value="1" name="role" id="rolAdmin">Administrador</option>
                                        <option value="2" name="role" id="rolAsesor">Asesor</option>
                                        <option value="3" name="role" id="rolJefe">Jefe de Oficina</option>
                                    </select>
                                </div>-->
                                
                                    
                              <div class="my-3 col-3 ">
                                    <label for="radioGroup">Rol</label>
                                    <br>
                                    <div class="btn-group " role="group" id="roleGroup"
                                        aria-label="Basic radio toggle button group" required>
    
                                        <input type="radio" class="btn-check" name="role" id="rolAdmin" value="1"
                                            autocomplete="off">
                                        <label for="rolAdmin" class="btn btn-outline-primary">Administrador</label>
    
                                        <input type="radio" class="btn-check" name="role" id="rolAsesor" value="2"
                                            autocomplete="off">
                                        <label for="rolAsesor" class="btn btn-outline-primary">Asesor</label>
    
                                        <input type="radio" class="btn-check" name="role" id="rolJefe" value="3"
                                            autocomplete="off">
                                        <label for="rolJefe" class="btn btn-outline-primary">Jefe Oficina</label>
                                    </div>
                                </div>
                            
                            </div>
                            <br>
                            <div class=" my-3 col-3 ">
                                <button type="submit" class="btn btn-primary submit">Guardar </button>
                                <button class="btn btn-secondary submit">Cancelar</button>
                            </div>
                        </form>
                     
                        
                    </div>
                </div>
            </div>            
        </div>


            <!--
            <div class="row mt-4">
                <h2  class="form-label m-1">Buscar Asesor</h2>
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <form action="/search" method="POST" role="search">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <input type="text" class="form-control" name="q" placeholder="106325147"> <span class="input-group-btn">
                                    <button type="submit" class="btn btn-outline-primary">
                                        <span class="glyphicon glyphicon-search">Buscar</span>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            -->

            <br>

            <div class="row">
<h1 for="exampleInputPassword1" class="form-label">Buscar Usuarios</h1>
<div class="col-md-6 grid-margin stretch-card">
    <div class="card">
    <form >
    <div class="input-group">
        <input type="text" class="form-control" name="search" id="search"
            placeholder="Busque al usuario por su nombre. Ej: Manuel Corrales"> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search">Buscar</span>
            </button>
        </span>
    </div>
</form>
  </div>
</div>

         <div class="row">
            <div class="col-md grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h6 class="card-title">Lista de Asesores</h6>
                    <div class="table-responsive">
                        <table class="table table-hover" >
                          <thead>
                            <tr>
                                <th>Cédula</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                          </thead>
                          <tbody>
                           <tbody id="resultado">
                                @foreach ($usuarios as $user)
                                    <tr>
                                        <td>{{ $user->cedula }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->apellido1 }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td class="text-center ">

                                            <form action="{{ route('userDelete', $user) }}" method="POST"
                                                class="inline-block">
                                                @csrf
                                                @method('delete')
                                                <a class="btn btn-warning "
                                                    href="{{ route('userShow', $user) }}">Modificar</a>
                                                
                                                <div class="modal fade" id="exampleModal" tabindex="-1"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    Adventencia</h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="btn-close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                ¿Está seguro de que desea eliminar el usuario de cédula
                                                                <b>#{{ $user->cedula }}</b>?
                                                                <br>
                                                                Nombre:<b> {{ $user->name }} {{ $user->apellido1 }}
                                                                    {{ $user->apellido2 }} </b>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Cerrar</button>
                                                                <button type="submit"
                                                                    class="btn btn-danger">Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>





            <!--
            <div class="card mt-4 px-0">
                <div class="card-header p-2">
                    <h2 class="form-label m-1" style="font-size:30px">Lista de Asesores</h6>
                </div>
                <div class="card-body m-2">

                    <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                            <thead>
                                <tr>
                                    <th>Cédula</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>

                                    <th>Correo</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($usuarios as $user)
                                    <tr>
                                        <td>{{ $user->cedula }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->apellido1 }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td class="text-center ">

                                            <form action="{{ route('userDelete', $user) }}" method="POST"
                                                class="inline-block">
                                                @csrf
                                                @method('delete')
                                                <a class="btn btn-warning "
                                                    href="{{ route('userShow', $user) }}">Modificar</a>
                                                
                                                <div class="modal fade" id="exampleModal" tabindex="-1"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    Adventencia</h5>
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="btn-close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                ¿Está seguro de que desea eliminar el usuario de cédula
                                                                <b>#{{ $user->cedula }}</b>?
                                                                <br>
                                                                Nombre:<b> {{ $user->name }} {{ $user->apellido1 }}
                                                                    {{ $user->apellido2 }} </b>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Cerrar</button>
                                                                <button type="submit"
                                                                    class="btn btn-danger">Eliminar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          <--  </div>-->
       <!-- </div>-->
    @endsection


    @push('plugin-scripts')
        <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>

        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/progressbar-js/progressbar.min.js') }}"></script>

        <script>
            $('#search').on('keyup', function(){
                var query = $(this).val();
                $.ajax({
                    method: 'POST',
                    url: '{{route("usuario.search")}}',
                    dataType: 'json',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        query: query,
                    },
                    success: function(res){
                        var tabla= '';
                        $('#resultado').html('');

                        $.each(res, function(index, value){
                            tabla =   ' <tr><th>'+value.cedula+'</th><td>'+value.name+'</td><td>'+value.apellido1+'</td><td>'+value.email+'</td><td><a class="btn btn-warning " href="{{ route('userShow', $user) }}">Modificar</a></td> </tr>';

                                        $('#resultado').append(tabla);
                        });
                    }
                });
            });
        </script>

    @endpush

    @push('custom-scripts')
        <script src="{{ asset('assets/js/data-table.js') }}"></script>

        <script src="{{ asset('assets/js/dashboard.js') }}"></script>
        <script src="{{ asset('assets/js/datepicker.js') }}"></script>
    @endpush
