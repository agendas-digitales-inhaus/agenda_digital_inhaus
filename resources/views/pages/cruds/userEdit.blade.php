@extends('layout.master')

@push('plugin-styles')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
@endpush


@section('content')

<div class="row">
    <div class="col-md-12 stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">Editar usuario</h6>
                <form action="{{ route('userUpdate', $user) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="row">

                        <div class="col-sm-6">
                            <label for="IdAsesor">Cédula</label>
                            <input type="number" class="form-control mr-4" id="IdAsesor" name="cedula"
                                autocomplete="off" value="{{ old('cedula', $user->cedula) }}">
                        </div>

                        <div class="col-sm-6">
                            <label for="NombreAsesor">Nombre</label>
                            <input name="name" type="text" class="form-control" id="NombreAsesor"
                                value="{{ old('id', $user->name) }}" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="ApellidoAsesor">Primer apellido</label>
                            <input type="text" name="apellido1" class="form-control" id="ApellidoAsesor"
                                autocomplete="off" value="{{ old('id', $user->apellido1) }}" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="ApellidoAsesor">Segundo apellido</label>
                            <input type="text" name="apellido2" class="form-control" id="ApellidoAsesor"
                                autocomplete="off" value="{{ old('id', $user->apellido2) }}" required>
                        </div>
                        <div class="col-sm-6">
                            <label for="CorreoAsesor">Correo</label>
                            <input name="email" type="email" class="form-control " id="CorreoAsesor" autocomplete="off"
                                value="{{ old('email', $user->email) }}" required>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 ">
                                <label for="radioGroup">Sede</label>
                                <br>
                                <!--   <div class="btn-group " role="group" id="radioGroup"
                                        aria-label="Basic radio toggle button group" required>
            
                                        <input type="radio" class="btn-check {{ $user->sede == 'heredia' ? 'active' : '' }}"
                                            {{ $user->sede == 'heredia' ? 'checked' : '' }} name="sede" id="SedeHeredia" value="heredia"
                                            autocomplete="off">
                                        <label for="SedeHeredia" class="btn btn-outline-primary">Heredia</label>
            
                                        <input type="radio" class="btn-check {{ $user->sede == 'sanjose' ? 'active' : '' }}"
                                            {{ $user->sede == 'sanjose' ? 'checked' : '' }} name="sede" id="SedeSanJose"
                                            value="sanjose" autocomplete="off">
                                        <label for="SedeSanJose" class="btn btn-outline-primary">San José</label>
            
                                        <input type="radio" class="btn-check {{ $user->sede == 'cartago' ? 'active' : '' }}"
                                            {{ $user->sede == 'cartago' ? 'checked' : '' }} name="sede" id="SedeCartago"
                                            value="cartago" autocomplete="off">
                                        <label for="SedeCartago" class="btn btn-outline-primary">Cartago</label>
            
            
                                    </div> -->
                                <select class="form-select" name="sede" aria-label=".form-select" required>

                                    <option {{ $user->sede == 'San José Norte' ? 'selected' : '' }}
                                        value="San José Norte">San José Norte</option>
                                    <option {{ $user->sede == 'San José Sur' ? 'selected' : '' }} value="San José Sur">
                                        San José Sur</option>
                                    <option {{ $user->sede == 'San José Este' ? 'selected' : '' }}
                                        value="San José Este">San José Este</option>
                                    <option {{ $user->sede == 'San José Oeste' ? 'selected' : '' }}
                                        value="San José Oeste">San José Oeste</option>
                                    <option {{ $user->sede == 'Cartago' ? 'selected' : '' }} value="Cartago">Cartago
                                    </option>
                                    <option {{ $user->sede == 'Heredia' ? 'selected' : '' }} value="Heredia">Heredia
                                    </option>
                                    <option {{ $user->sede == 'Comercial' ? 'selected' : '' }} value="Comercial">
                                        Comercial</option>


                                </select>
                            </div>

                            <div class="col-sm-6">
                                <label for="radioGroup">Rol</label>
                                <br>
                                <div class="btn-group " role="group" id="roleGroup"
                                    aria-label="Basic radio toggle button group" required>

                                    <input type="radio" class="btn-check  {{ $user->role == '1' ? 'active' : '' }}"
                                        {{ $user->role == '1' ? 'checked' : '' }} name="role" id="rolAdmin" value="1"
                                        autocomplete="off">
                                    <label for="rolAdmin" class="btn btn-outline-primary">Administrador</label>

                                    <input type="radio" class="btn-check {{ $user->role == '2' ? 'active' : '' }}"
                                        {{ $user->role == '2' ? 'checked' : '' }} name="role" id="rolAsesor" value="2"
                                        autocomplete="off">
                                    <label for="rolAsesor" class="btn btn-outline-primary">Asesor</label>

                                    <input type="radio" class="btn-check {{ $user->role == '3' ? 'active' : '' }}"
                                        {{ $user->role == '3' ? 'checked' : '' }} name="role" id="rolJefe" value="3"
                                        autocomplete="off">
                                    <label for="rolJefe" class="btn btn-outline-primary">Jefe de Oficina</label>
                                </div>
                                <br>
                                <br>
                                <br>

                            </div>

                        </div>

                        <div class="float-right">

                            
                                <a href="{{ route('users') }}" class=" btn btn-secondary">Cancelar</a>

                                <button type="submit" class=" btn btn-outline-primary">Actualizar</button>

                                <button type="button" class=" btn btn-danger" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal">
                                    Eliminar
                                </button>
                            

                        </div>
                </form>
                
                {{-- ES EL BOTON DE ELIMINAR PERO HAY QUE AGREGARLE ESTILOS --}}
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Advertencia</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="btn-close"></button>
                            </div>
                            <div class="modal-body">
                                ¿Está seguro de que desea eliminar el usuario de cédula <b>#{{ $user->cedula }}</b>?
                                <br>
                                Nombre:<b> {{ $user->name }} {{ $user->apellido1 }} {{ $user->apellido2 }} </b>
                            </div>
                            <div class="modal-footer">
                                <form class="d-flex flex-row-reverse" action="{{ route('userDelete', $user) }}"
                                    method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection


@push('plugin-scripts')
<script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>

<script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/plugins/progressbar-js/progressbar.min.js') }}"></script>
@endpush

@push('custom-scripts')
<script src="{{ asset('assets/js/data-table.js') }}"></script>

<script src="{{ asset('assets/js/dashboard.js') }}"></script>
<script src="{{ asset('assets/js/datepicker.js') }}"></script>
@endpush