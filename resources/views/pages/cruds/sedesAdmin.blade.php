@extends('layout.master')

@push('plugin-styles')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />

@endpush


@section('content')

<div class="col-12 col-xl-12 stretch-card">
    <div class="row flex-grow-1">
        <div class="row">
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                            <h6 class="card-title mb-0">Sedes Existentes</h6>

                        </div>
                        <div class="row">
                            <div class="col-6 col-md-12 col-xl-5">
                                <h3 class="mb-2">3</h3>
                                <div class="d-flex align-items-baseline">
                                    <p class="text-success">
                                        <span>+33.3%</span>
                                        <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                    </p>
                                </div>
                            </div>
                            <div class="col-6 col-md-12 col-xl-7">
                                <div id="customersChart" class="mt-md-3 mt-xl-0"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                            <h6 class="card-title mb-0"> Citas de mi Sede Este mes</h6>

                        </div>
                        <div class="row">
                            <div class="col-6 col-md-12 col-xl-5">
                                <h3 class="mb-2">24</h3>
                                <div class="d-flex align-items-baseline">
                                    <p class="text-success">
                                        <span>+6.2%</span>
                                        <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-baseline">
                            <h6 class="card-title mb-0"> Asesores en mi Sede</h6>

                        </div>
                        <div class="row">
                            <div class="col-6 col-md-12 col-xl-5">
                                <h3 class="mb-2">6</h3>
                                <div class="d-flex align-items-baseline">
                                    <p class="text-secondary">
                                        <span>+0.0%</span>
                                        <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="card ">
            <div class="card-header p-2">
                <h2 class="form-label m-1">Estadísticas Asesores Sede de San José</h2>
            </div>
            <div class="card-body">


                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nombre Completo</th>
                                <th>Citas mes anterior</th>
                                <th>Citas mes actual</th>
                                <th>Variación</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Beto Salas Pedregal</th>
                                <td>6</td>
                                <td>9</td>
                                <td class="text text-success">+ 50.0%</td>
                            </tr>
                            <tr>
                                <th>Sofía Venegas Guido</th>
                                <td>20</td>
                                <td>15</td>
                                <td  class="text text-danger">- 20.0%</td>
                            </tr>
                            <tr>
                                <th>Jorge Quirós Valverde</th>
                                <td>6</td>
                                <td>4</td>
                                <td class="text text-danger">- 50.0%</td>
                            </tr>
                            <tr>
                                <th>Vera Martínez Muñoz</th>
                                <td>9</td>
                                <td>9</td>
                                <td class="text text-secondary">+ 0.0</td>
                            </tr>
                            <tr>
                                <th>Rolando Carmona Fallas</th>
                                <td>12</td>
                                <td>13</td>
                                <td class="text text-success">+ 8.33%</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            </div>

            <br>
            <!--
        <div class="row mt-4">
            <h2  class="form-label m-1">Buscar Asesor</h2>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <form action="/search" method="POST" role="search">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="106325147"> <span class="input-group-btn">
                                <button type="submit" class="btn btn-outline-primary">
                                    <span class="glyphicon glyphicon-search">Buscar</span>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        -->

            <br>
            <div class="card mt-4">
                <div class="card-header p-2">
                    <h2 class="form-label m-1" style="font-size:30px">Citas de Sede San José</h6>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                            <thead>
                                <tr>
                                    <th>Cédula del Cliente</th>
                                    <th>ID de la Propiedad</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>ID del Lead</th>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>118050243</td>
                                    <td>150</td>
                                    <td>Marco</td>
                                    <td>Vargas</td>
                                    <td>5</td>
                                    <td>15 de noviembre 2022</td>
                                    <td>8:00 a.m</td>

                                    <td class="text-center">
                                        <button class="col btn btn-warning">Modificar</button>
                                        <button class="col btn btn-danger">Eliminar</button>
                                    </td>

                                </tr>
                                <tr>
                                    <td>107350579</td>
                                    <td>150</td>
                                    <td>Javier</td>
                                    <td>Solis</td>
                                    <td>8</td>
                                    <td>10 de Octubre 2022</td>
                                    <td>8:00 a.m</td>

                                    <td class="text-center">
                                        <button class="col btn btn-warning">Modificar</button>
                                        <button class="col btn btn-danger">Eliminar</button>
                                    </td>

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        @endsection


        @push('plugin-scripts')
        <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>

        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/progressbar-js/progressbar.min.js') }}"></script>

        @endpush

        @push('custom-scripts')
        <script src="{{ asset('assets/js/data-table.js') }}"></script>

        <script src="{{ asset('assets/js/dashboard.js') }}"></script>
        <script src="{{ asset('assets/js/datepicker.js') }}"></script>

        @endpush
