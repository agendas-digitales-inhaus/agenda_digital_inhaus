
@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@endpush


@section('content')
<div class="row">
<h6 class="card-title" style="font-size: 30px">Modificar Cita</h6>
<div class="col-md-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
       
        <form class="forms-sample">
          <div class="mb-3">
            <label for="exampleInputUsername1" class="form-label">Cedula del cliente</label>
            <input type="number" class="form-control" id="IdCliente" autocomplete="off" placeholder="118050243">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Id de la propiedad</label>
            <input type="number" class="form-control" id="IdPropiedad" placeholder="150">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Nombre del lead</label>
            <input type="text" class="form-control" id="NombreLead" autocomplete="off" placeholder="Mark">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Apellido</label>
            <input type="text" class="form-control" id="NombreLead" autocomplete="off" placeholder="Otto">
          </div>
          <div class="mb-3">
            <label for="exampleInputUsername1" class="form-label">Precio de la propiedad</label>
            <input type="number" class="form-control" id="PrecioPropiedad" autocomplete="off" placeholder="150.000">
          </div>
          <div class="mb-3">
            <label for="exampleInputUsername1" class="form-label">Comision</label>
            <input type="number" class="form-control" id="Comision" autocomplete="off" placeholder=" 2.5%">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Direccion</label>
            <input type="text" class="form-control" id="Direccion" autocomplete="off" placeholder="San isidro de coronado">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Hora de la cita</label>
            <input type="text" class="form-control" id="Direccion" autocomplete="off" placeholder="8:00 Am">
          </div>
          <div class="card-body">
          <label for="exampleInputPassword1" class="form-label">Fecha de la cita</label>
        <div class="input-group date datepicker" id="datePickerExample">
          <input type="text" class="form-control">
          <span class="input-group-text input-group-addon"><i data-feather="calendar"></i></span>
        </div>
      </div>
         
      <button type="submit" class="btn btn-success">Modificar</button>
          <button class="btn btn-secondary">Cancel</button>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection


@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/chartjs/chart.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.js') }}"></script>
  <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/progressbar-js/progressbar.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/dashboard.js') }}"></script>
  <script src="{{ asset('assets/js/datepicker.js') }}"></script>
@endpush