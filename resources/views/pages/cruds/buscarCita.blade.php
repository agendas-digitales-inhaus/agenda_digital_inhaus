
@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@endpush


@section('content')
<div class="row">
<h1 for="exampleInputPassword1" class="form-label">Buscar Cita</h1>
<div class="col-md-6 grid-margin stretch-card">
    <div class="card">
    <form action="/search" method="POST" role="search">
    {{ csrf_field() }}
    <div class="input-group">
        <input type="text" class="form-control" name="q"
            placeholder="Cedula del cliente o Id de la propiedad"> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</form>
  </div>
</div>

</hr>

<div class="row">
  <div class="col-md-6 grid-margin stretch-card" style="width: 1300px">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title">Resultado de busqueda</h6>
        <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>Nombre Del cliente</th>
                  <th>Id de la propiedad</th>
                  <th>Correo</th>
                  <th>telefono</th>
                  <th>Fecha</th>
                </tr>
              </thead>
              <tbody>
                @foreach($citas as $cita)
                <tr>
                  <th>{{ $cita->nombreLead }}</th>
                  <td>{{ $cita->IdPropiedades }}</td>
                  <td>{{ $cita->correo }}</td>
                  <td>{{ $cita->telefono }}</td>
                  <td>{{ $cita->inicio }}</td>
                 
                  <td><a href="{{ url('/cruds/modificarCita') }}" type="button" class="btn btn-warning">Modificar Cita</a></td>
                </tr>
                @endforeach
                
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/chartjs/chart.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.js') }}"></script>
  <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
  <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/progressbar-js/progressbar.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/dashboard.js') }}"></script>
  <script src="{{ asset('assets/js/datepicker.js') }}"></script>
@endpush