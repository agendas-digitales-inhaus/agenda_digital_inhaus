@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/fullcalendar/main.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-3 d-none d-md-block">
        <div class="card">
          <div class="card-body">
            <h6 class="card-title mb-4">Full calendar</h6>
            <div id='external-events' class='external-events'>
              <h6 class="mb-2 text-muted">Draggable Events</h6>
              <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>
                <div class='fc-event-main'>Birth Day</div>
              </div>
              <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>
                <div class='fc-event-main'>New Project</div>
              </div>
              <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>
                <div class='fc-event-main'>Anniversary</div>
              </div>
              <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>
                <div class='fc-event-main'>Clent Meeting</div>
              </div>
              <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event'>
                <div class='fc-event-main'>Office Trip</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-9">
        <div class="card">
          <div class="card-body">
            <div id='fullcalendar'></div>
          </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div id="fullCalModal" class="modal fade">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="modalTitle1" class="modal-title"></h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal"><span class="visually-hidden">close</span></button>
      </div>
      <div id="modalBody1" class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-primary">Event Page</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="bookingModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendar Cita</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="" id="formulario">
      <div class="modal-body">
        <label for="nombreLead">Nombre del lead</label>
        <input type="text" class="form-control" id="nombreLead" required>
        <span id="nombreLeadError" class="text-danger"></span>
        <label for="telefono">Telefono</label>
        <input type="text" class="form-control" id="telefono" required>
        <span id="telefonoError" class="text-danger"></span>
        <label for="correo">correo</label>
        <input type="text" class="form-control" id="correo" required>
        <span id="correoError" class="text-danger"></span>
        <label for="idPropiedades">idPorpiedades</label>
        <input type="text" class="form-control" id="idPropiedades" required>
        <span id="idPorpiedadesError" class="text-danger"></span>
        <label for="color">Color</label>
        <input type="color" class="form-control" id="color" required>
        <span id="colorError" class="text-danger"></span>
      
        <label for="Inicio" id="iniciolabel">Inicio</label>
        <input type="datetime-local" class="form-control" id="Inicio" required>
        <span id="colorError" class="text-danger"></span>
        <label for="Fin" id="finlabel">Fin</label>
        <input type="datetime-local" class="form-control" id="Fin" required>
        <span id="colorError" class="text-danger"></span>
        <input type="hidden" id="eventId" name="event_id">
      </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-warning" id="btnModificar">Modificar</button>
        <button type="button" class="btn btn-danger" id="btnBorrar">Borrar</button>
        <button type="submit" class="btn btn-primary" id="saveBtn">Agendar</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/fullcalendar/main.min.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/fullcalendar.js') }}"></script>
@endpush