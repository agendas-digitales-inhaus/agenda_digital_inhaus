@extends('layout.masterAsesor')

@section('content')



<div class="row">
  <div class="col-md-12 stretch-card">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title">Editar Cita</h6>
          <form action="{{ route('asesor.update', $cita->id) }}" Method="POST">
              @method('PATCH')
            <div class="row">
              <div class="col-sm-6">
                <div class="mb-3">
                  <label class="form-label">Nombre del Lead</label>
                  <input type="text" class="form-control" value="{{$cita->nombreLead}}" placeholder="Enter first name">
                </div>
              </div><!-- Col -->
              <div class="col-sm-6">
                <div class="mb-3">
                  <label class="form-label">Telefono</label>
                  <input type="text" class="form-control" value="{{$cita->telefono}}" placeholder="Enter last name">
                </div>
              </div><!-- Col -->
            </div><!-- Row -->
            <div class="row">
              <div class="col-sm-4">
                <div class="mb-3">
                  <label class="form-label">Correo</label>
                  <input type="text" class="form-control" value="{{$cita->correo}}" placeholder="Enter city">
                </div>
              </div><!-- Col -->
              <div class="col-sm-4">
                <div class="mb-3">
                  <label class="form-label">Propiedades visitadas</label>
                  <input type="text" class="form-control" value="{{$cita->idPropiedades}}" placeholder="Enter state">
                </div>
              </div><!-- Col -->
              <div class="col-sm-4">
                <div class="mb-3">
                  <label class="form-label">color</label>
                  <input type="color" class="form-control" value="{{$cita->color}}" placeholder="Enter zip code">
                </div>
              </div><!-- Col -->
            </div><!-- Row -->
            <div class="row">
              <div class="col-sm-6">
                <div class="mb-3">
                  <label class="form-label">Inicio de cita</label>
                  <input type="datetime" class="form-control" value="{{$cita->inicio}}" placeholder="Enter email">
                </div>
              </div><!-- Col -->
              <div class="col-sm-6">
                <div class="mb-3">
                  <label class="form-label">Fin de cita</label>
                  <input type="datetime" class="form-control" value="{{$cita->fin}}" autocomplete="off" placeholder="Password">
                </div>
              </div><!-- Col -->
            </div><!-- Row -->
          </form>
          <button type="submit" class="btn btn-primary submit">Submit form</button>
      </div>
    </div>
  </div>
</div>
@endsection
