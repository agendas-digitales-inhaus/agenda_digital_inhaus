@extends('layout.masterJefeOficina')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@endpush


@section('content')

    <div class="row"!>
        <h1 for="exampleInputPassword1" class="form-label">Buscar Cita</h1>
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" id="search"
                            placeholder="Busque por medio del nombre del cliente"> <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">
                                <span class="">Buscar</span>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>

        </hr>

        


        <div class="col-md grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h6 class="card-title">Lista de Citas</h6>
                    <div class="table-responsive">
                        <table class="table table-hover" >
                          <thead>
                          <tr>
                                        <th>Nombre Del cliente</th>
                                        <th>Id de la propiedad</th>
                                        <th>Correo</th>
                                        <th>teléfono</th>
                                        <th>Fecha</th>
                                        <th>Opciones</th>
                                    </tr>
                          </thead>
                          <tbody>
                          <tbody id="resultado">
                          @forelse ($users as $cita)
                                        <tr>
                                            <th>{{ $cita->nombreLead }}</th>
                                            <td>{{ $cita->idPropiedades }}</td>
                                            <td>{{ $cita->correo }}</td>
                                            <td>{{ $cita->telefono }}</td>
                                            <td>{{ $cita->inicio }}</td>

                                            <td><a class="btn btn-warning "
                                                    href="{{ route('asesor.editarCita', $cita->id) }}">Modificar</a></td>
                                        </tr>
                                        @empty 
                                        <tr>
                                            <th>No hay datos</th>
                                            <td>No hay datos</td>
                                            <td>No hay datos</td>
                                            <td>No hay datos</td>
                                            <td>No hay datos</td>
                                        </tr>
                                    @endforelse
                            </tbody>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
    @endsection


    @push('plugin-scripts')
        <script src="{{ asset('assets/plugins/chartjs/chart.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/progressbar-js/progressbar.min.js') }}"></script>
        
        <script>
            $('#search').on('keyup', function(){
                var query = $(this).val();
                $.ajax({
                    method: 'POST',
                    url: '{{route("asesor.search")}}',
                    dataType: 'json',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        query: query,
                    },
                    success: function(res){
                        var tabla= '';
                        $('#resultado').html('');

                        $.each(res, function(index, value){
                            tabla =   ' <tr><th>'+value.nombreLead+'</th><td>'+value.idPropiedades+'</td><td>'+value.correo+'</td><td>'+value.telefono+'</td><td>'+value.inicio+'</td> </tr>';

                                        $('#resultado').append(tabla);
                        });
                    }
                });
            });
        </script>
       
    @endpush

    @push('custom-scripts')
        <script src="{{ asset('assets/js/dashboard.js') }}"></script>
        <script src="{{ asset('assets/js/datepicker.js') }}"></script>
    @endpush
