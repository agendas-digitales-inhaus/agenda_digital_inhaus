@extends('layout.master')

@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    @endpush


@section('content')

<div class="container">
<div class="row">

<div class=" grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title">Citas de cada asesor en el mes actual</h6>
        <div id="donutchart"></div>
      </div>
    </div>
  </div>

  <div class=" grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title">Cantidad de citas por mes de la oficina</h6>
        <div id="chart"></div>
      </div>
    </div>
  </div>

                

</div>
</div>

        

    @endsection


    @push('plugin-scripts')
        <script src="{{ asset('assets/plugins/chartjs/chart.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/progressbar-js/progressbar.min.js') }}"></script>
        
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script src="{{ asset('assets/plugins/apexcharts/apexcharts.min.js') }}"></script> 
    
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          <?php echo $Data?>
        ]);

        var options = {
            height: 450,
          pieHole: 0.2,
        };

        

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>

<script type="text/javascript">
    var enero = @json($enero);
    console.log(enero);
    var options = {
          series: [{
          data: [{{count($enero)}}, {{count($febrero)}}, {{count($marzo)}}, {{count($abril)}},
          {{count($mayo)}}, {{count($junio)}}, {{count($julio)}}, {{count($agosto)}},
          {{count($septiembre)}}, {{count($octubre)}}, {{count($noviembre)}}, {{count($diciembre)}}]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            dataLabels: {
        orientation: 'vertical',
        position: 'center' // bottom/center/top
      }
          }
        },
        dataLabels: {
            orientation: 'horizontal',
          enabled: false
        },
        xaxis: {
          categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
            'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
          ],
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
  </script>
       
    @endpush

    @push('custom-scripts')
        <script src="{{ asset('assets/js/dashboard.js') }}"></script>
        <script src="{{ asset('assets/js/datepicker.js') }}"></script>

    @endpush
