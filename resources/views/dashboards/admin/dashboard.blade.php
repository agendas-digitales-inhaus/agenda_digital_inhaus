@extends('layout.master')


@section('styles')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
<link href="library/bootstrap-5/bootstrap.min.css" rel="stylesheet" />



@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            {{-- <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
        <h3>Hola Admin {{Auth::user()->name}}</h3>

        <div class="card-body">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            {{ __('Bienvenido de vuelta!') }}

            <button type="button" name="" id="" class="btn btn-primary" btn-lg btn-block"><a class="dropdown-item"
                    href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Salir') }}
                </a></button>

        </div>
    </div>
</div> --}}
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="bookingModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agendar Cita</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" id="formulario">
                <div class="modal-body">
                    <label for="nombreLead">Nombre del cliente</label>
                    <input type="text" class="form-control" id="nombreLead" placeholder="Samuel Rodriguez" required>
                    @error('nombreLead')
                    <span class="text-danger"> {{$message}} </span>
                    @enderror

                    <label for="telefono">Teléfono del cliente<small class="text-secondary"> (Opcional)</small></label>
                    <input type="text" class="form-control" id="telefono" placeholder="87425539">

                    <label for="correo">Correo del cliente<small class="text-secondary"> (Opcional)</small></label>
                    <input type="text" class="form-control" id="correo" placeholder="s.rodriguez@inhauscr.com">

                    <label for="idPropiedades">Propiedades que van a visitar</label>
                    <input type="text" class="form-control" id="idPropiedades" placeholder="29867 / 30875" required>
                    @error('idPropiedades')
                    <span class="text-danger"> {{$message}} </span>
                    @enderror

                    <label for="color">Color de la cita</label>
                    <input type="color" class="form-control" id="color" required>

                    @error('color')
                    <span class="text-danger"> {{$message}} </span>
                    @enderror



                    <label for="Inicio" id="iniciolabel">Inicio de la cita</label>
                    <input type="datetime-local" class="form-control" id="Inicio" required>
                    @error('inicio')
                    <span class="text-danger"> {{$message}} </span>
                    @enderror


                    <label for="Fin" id="finlabel">Fin de la cita</label>
                    <input type="datetime-local" class="form-control" id="Fin" required>
                    @error('fin')
                    <span class="text-danger"> {{$message}} </span>
                    @enderror

                    <input type="hidden" id="eventId" name="event_id">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-warning" id="btnModificar">Modificar</button>
                    <button type="button" class="btn btn-danger" id="btnBorrar">Borrar</button>
                    <button type="submit" class="btn btn-primary" id="saveBtn">Agendar</button>
                </div>
            </form>

        </div>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="card p-0">
            <div class="card-header">
                <h2 class="text-center">Calendario</h2>
            </div>
            <div class="card-body">
                <form class="form " action="" method="Get">
                    <strong><label for="user_id">Lista de Asesores</label></strong>
                    <label for="">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <select class="form-control ml-4" style="width: 200px;" name="user_id" id="user_id">
                        <option value="">Seleccione el nombre</option>
                        @foreach ( $filtro as $fil)
                        <option value="{{ $fil->id }}" @if (request('user_id')==$fil->id) selected @endif
                            >{{ $fil->name }}</option>
                        @endforeach
                    </select>
                    <label for="">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <button type="submit" class="btn btn-sm btn-primary">Filtrar</button>


                </form>

                <div class=" mt-5 mb-5">


                    <div id="calendar">


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@endsection






@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
    integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
</script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var bookings = @json($events);
    console.log(bookings);
    var nombre = @json($filtro);
    console.log(nombre);

    $('#calendar').fullCalendar({
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov',
            'Dic'
        ],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
        header: {
            left: 'prev, next, today',
            center: 'title',
            right: '',
        },
        eventColor: '#378006',
        eventTextColor: '#ffffff',
        events: bookings,
        allDays: false,
        selectable: true,
        selectHelper: true,



        select: function(start, end) {
            $('#bookingModal').modal('toggle');
            $('#btnModificar').hide();
            $('#btnBorrar').hide();
            $('#Inicio').show();
            $('#Fin').show();
            $('#iniciolabel').show();
            $('#finlabel').show();


            $('#saveBtn').click(function() {

                var nombreLead = $('#nombreLead').val();
                var telefono = $('#telefono').val();
                var correo = $('#correo').val();
                var idPropiedades = $('#idPropiedades').val();
                var color = $('#color').val();
                var inicio = $("#Inicio").val();
                var fin = $("#Fin").val();
                console.log(inicio);

                $.ajax({
                    url: "{{route('citas.store')}}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        nombreLead,
                        telefono,
                        correo,
                        idPropiedades,
                        color,
                        inicio,
                        fin
                    },
                    success: function(response) {
                        /*
                        console.log(response)
                        */

                        swal("Confirmado", "Cita agendada con exito",
                            "success");
                        $('#bookingModal').modal('hide');
                        $('#calendar').fullCalendar('renderEvent', {
                            'title': response.nombreLead,
                            'start': response.inicio,
                            'end': response.fin
                        })

                    },
                    error: function(response) {
                        console.log(response)
                    },
                });
            })
        },

        editable: true,
        eventDrop: function(event) {
            var id = event.id;
            var inicio = moment(event.start).format('YYYY-MM-DD HH-mm-ss');
            var fin = moment(event.end).format('YYYY-MM-DD HH-mm-ss');

            $.ajax({
                url: "{{ route('citas.update', '') }}" + '/' + id,
                type: "PATCH",
                dataType: "json",
                data: {
                    inicio,
                    fin
                },
                success: function(response) {
                    swal("Listo", "Cita modificada", "success");
                    console.log(response);
                },
                error: function(error) {
                    console.log(error);
                },
            });

        },
        eventResize: function(event) {
            var id = event.id;
            var inicio = moment(event.start).format('YYYY-MM-DD HH-mm-ss');
            var fin = moment(event.end).format('YYYY-MM-DD HH-mm-ss');

            $.ajax({
                url: "{{ route('citas.update', '') }}" + '/' + id,
                type: "PATCH",
                dataType: "json",
                data: {
                    inicio,
                    fin
                },
                success: function(response) {
                    swal("Listo", "Cita modificada", "success");
                    console.log(response);
                },
                error: function(error) {
                    console.log(error);
                },
            });

        },
        eventClick: function(event) {
            $("#nombreLead").val(event.title);
            $("#telefono").val(event.telefono);
            $("#correo").val(event.correo);
            $("#idPropiedades").val(event.idPropiedades);
            $("#color").val(event.color);
            $("#eventId").val(event.id);
            $('#Inicio').hide();
            $('#Fin').hide();
            $('#iniciolabel').hide();
            $('#finlabel').hide();
            $('#btnModificar').show();
            $('#btnBorrar').show();

            $('#bookingModal').modal('show');


            document.getElementById("btnBorrar").addEventListener("click", function() {
                var id = event.id;
                var user_id = event.user_id;
                if (confirm('Seguro que quiere borrar esta cita?')) {
                    $.ajax({
                        url: "{{ route('citas.destroy', '') }}" + '/' + id,
                        type: "DELETE",
                        dataType: "json",
                        data: {
                            user_id
                        },
                        success: function(response) {
                            $("#calendar").fullCalendar('removeEvents',
                                response)
                            swal("Listo", "Cita eliminada!!!", "success");
                            $('#bookingModal').modal('hide');
                            console.log(response);
                        },
                        error: function(error) {
                            swal("Error",
                                "No tiene permiso para modificar esta cita",
                                "error");
                            $('#bookingModal').modal('hide');
                        },
                    });
                }
            });

            document.getElementById("btnModificar").addEventListener("click", function() {
                var id = event.id;
                var user_id = event.user_id;
                var nombreLead = $("#nombreLead").val();
                var telefono = $("#telefono").val();
                var correo = $("#correo").val();
                var idPropiedades = $("#idPropiedades").val();
                var color = $("#color").val();
                console.log(user_id);
                if (confirm('Seguro que quiere modificar esta cita?')) {
                    $.ajax({
                        url: "{{ route('citas.updateDatos', '') }}" + '/' + id,
                        type: "PATCH",
                        dataType: "json",
                        data: {
                            nombreLead,
                            telefono,
                            correo,
                            idPropiedades,
                            color,
                            user_id
                        },
                        success: function(response) {
                            swal("Listo",
                                "Cita modificada refresque para ver los cambios",
                                "success");
                            $('#bookingModal').modal('hide');
                            console.log(user_id);
                        },
                        error: function(error) {
                            swal("Error",
                                "No tiene permiso para modificar esta cita",
                                "error");
                            $('#bookingModal').modal('hide');
                        },
                    });
                }
            });



        },


    });

    $('#bookingModal').on("hidden.bs.modal", function() {

        $('#saveBtn').unbind();
        $(this).find('form').trigger('reset');

    });





});
</script>

<script>

$('#user_id').select2();

</script>



@endsection