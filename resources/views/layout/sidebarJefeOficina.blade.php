<nav class="sidebar">
  <div class="sidebar-header">
  <a style="color: #f47732" href="{{ url('/jefeOficina/dashboard') }}" class="sidebar-brand">
      Inhaus<span style="color: black">Agendas</span>
    </a>
    <div class="sidebar-toggler active">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <div class="sidebar-body">
    <ul class="nav">
      <li class="nav-item nav-category">Principal</li>
      <li class="nav-item {{ active_class(['/']) }}">
        <a href="{{ url('/jefeOficina/dashboard') }}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">Dashboard</span>
        </a>

      </li>
      <li class="nav-item {{ active_class(['cruds/calendar']) }}">
      <a href="{{ url('/jefeOficina/Sede') }}" class="nav-link">
          <i class="link-icon" data-feather="home"></i>
          <span class="link-title">Sede</span>
        </a>
      </li>
      <li class="nav-item nav-category">Citas</li>
        <li class="nav-item {{ active_class(['cruds/calendar']) }}">
        <a href="{{ url('/jefeOficina/buscarCita') }}" class="nav-link">
          <i class="link-icon" data-feather="search"></i>
          <span class="link-title">Buscar Cita</span>
        </a>
      </li>
      <li class="nav-item {{ active_class(['apps/calendar']) }}">
      <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="link-icon" data-feather="unlock"></i>
          <span class="link-title">Cerrar sesion</span>
                                    </a>
      </li>
      </li>
      </li>

</nav>
