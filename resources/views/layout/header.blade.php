<nav class="navbar" style="position: absolute;">
  <a href="#" class="sidebar-toggler">
    <i data-feather="menu"></i>
  </a>
  <div class="navbar-content">
    <form class="search-form">
      <div class="input-group">
        <div class="input-group-text">
        Agendas Digitales InHaus
        </div>

      </div>
    </form>
    <ul class="navbar-nav">

    
      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
    </ul>
  </div>
</nav>
