@yield('scripts')
<footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between px-4 py-3 border-top small">
  <p class="text-muted mb-1 mb-md-0">Copyright © 2022 <a href="https://inhauscr.com/" target="_blank">Inhaus Real Estate</a>.</p>
</footer>