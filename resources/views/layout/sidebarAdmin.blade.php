<nav class="sidebar">
  <div class="sidebar-header">
    <a style="color: #f47732" href="{{ url('/admin/dashboard') }}" class="sidebar-brand">
      Inhaus<span style="color: black">Agendas</span>
    </a>
    <div class="sidebar-toggler active">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <div class="sidebar-body">
    <ul class="nav">
      <li class="nav-item nav-category">Principal</li>
      <li class="nav-item {{ active_class(['/']) }}">
        <a href="{{ url('/admin/dashboard') }}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">Dashboard</span>
        </a>

      </li>
      <li class="nav-item {{ active_class(['cruds/calendar']) }}">
      <a href="{{route('users')}}" class="nav-link">
          <i class="link-icon" data-feather="users"></i>
          <span class="link-title">Usuarios</span>
        </a>
      </li>
      

      <li class="nav-item {{ active_class(['auth/*']) }}">
        <a class="nav-link" data-bs-toggle="collapse" href="#auth" role="button" aria-expanded="{{ is_active_route(['auth/*']) }}" aria-controls="auth">
        <i class="link-icon" data-feather="home"></i>
          <span class="link-title">Sedes</span>
          <i class="link-arrow" data-feather="chevron-down"></i>
        </a>
        <div class="collapse {{ show_class(['auth/*']) }}" id="auth">
          <ul class="nav sub-menu">
            <li class="nav-item">
              <a href="{{ url('/admin/SanJoseEste') }}" class="nav-link {{ active_class(['auth/login']) }}">San José Este</a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/admin/SanJoseNorte') }}" class="nav-link {{ active_class(['auth/register']) }}">San José Norte</a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/admin/SanJoseSur') }}" class="nav-link {{ active_class(['auth/register']) }}">San José Sur</a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/admin/SanJoseOeste') }}" class="nav-link {{ active_class(['auth/register']) }}">San José Oeste</a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/admin/Heredia') }}" class="nav-link {{ active_class(['auth/register']) }}">Heredia</a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/admin/Comercial') }}" class="nav-link {{ active_class(['auth/register']) }}">Comercial</a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/admin/Cartago') }}" class="nav-link {{ active_class(['auth/register']) }}">Cartago</a>
            </li>
        </div>
      </li>

      <li class="nav-item nav-category">Citas</li>
      
        <li class="nav-item {{ active_class(['cruds/calendar']) }}">
        <a href="{{ url('/admin/buscarCita') }}" class="nav-link">
          <i class="link-icon" data-feather="search"></i>
          <span class="link-title">Buscar Cita</span>
        </a>
      </li>

      
      <li class="nav-item {{ active_class(['apps/calendar']) }}">
      <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="link-icon" data-feather="unlock"></i>
          <span class="link-title">Cerrar sesion</span>
                                    </a>
      </li>
      </li>
      </li>
     

      

      

</nav>
