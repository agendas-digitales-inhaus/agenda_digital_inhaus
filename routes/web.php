<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AsesorController;
use App\Http\Controllers\CitasController;
use App\Http\Controllers\JefeDeOficinaController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;

//Usando controllers
/*     Route::resource('users', UserController::class ); */
Route::get('usuarios', [UserController::class, 'index'])->name('users');
Route::post('usuarios', [UserController::class, 'store'])->name('usersStore');
Route::get('usuarios/{user}', [UserController::class, 'show'])->name('userShow');
Route::put('usuarios/{user}', [UserController::class, 'update'])->name('userUpdate');
Route::delete('usuarios/{user}',[UserController::class, 'destroy'])->name('userDelete');
Route::post('/search', [UserController::class, 'search'])->name('usuario.search');


//fullcalenfarprueba
Route::get('calendar-event', [CalenderController::class, 'index']);
Route::post('calendar-crud-ajax', [CalenderController::class, 'calendarEvents']);

Route::get('/', function () {
    return view('login');
});


Auth::routes(['verify' => true]);

Route::group(['prefix' => 'admin'], function () {
    Route::get('dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('profile', [AdminController::class, 'profile'])->name('admin.profile');
    Route::post('citas', [AdminController::class, 'store'])->name('admin.store');
    Route::get('buscarCita', [AdminController::class, 'indexBuscarCitas'])->name('admin.buscarCita');
    Route::get('SanJoseEste', [AdminController::class, 'SanJoseEste'])->name('admin.SanJoseEste');
    Route::get('SanJoseNorte', [AdminController::class, 'SanJoseEste'])->name('admin.SanJoseEste');
    Route::get('SanJoseSur', [AdminController::class, 'SanJoseSur'])->name('admin.SanJoseSur');
    Route::get('SanJoseOeste', [AdminController::class, 'SanJoseOeste'])->name('admin.SanJoseOeste');
    Route::get('Heredia', [AdminController::class, 'Heredia'])->name('admin.Heredia');
    Route::get('Comercial', [AdminController::class, 'Comercial'])->name('admin.Comercial');
    Route::get('Cartago', [AdminController::class, 'Cartago'])->name('admin.Cartago');

});



Route::group(['prefix' => 'jefeOficina'], function () {
    Route::get('dashboard', [JefeDeOficinaController::class, 'index'])->name('jefeOficina.dashboard');
    Route::get('buscarCita', [JefeDeOficinaController::class, 'indexBuscarCitas'])->name('jefeOficina.buscarCita');
    Route::post('citas', [JefeDeOficinaController::class, 'store'])->name('jefeOficina.store');
    Route::get('Sede', [JefeDeOficinaController::class, 'Sede'])->name('jefeOficina.Sede');
});



Route::group(['prefix' => 'asesor'], function () {
    Route::get('dashboard', [AsesorController::class, 'indexAsesor'])->name('asesor.dashboard');
    Route::get('profile', [AsesorController::class, 'profile'])->name('asesor.profile');
    Route::post('citas', [AsesorController::class, 'store'])->name('asesor.store');
    Route::get('buscarCita', [AsesorController::class, 'indexBuscarCitas'])->name('asesor.buscarCita');
    /*Route::get('citas/{cita}', [AsesorController::class, 'show'])->name('citaShow');*/
    Route::post('asesor/update/{id}', [AsesorController::class, 'update'])->name('asesor.editarCita');
    Route::post('/search', [AsesorController::class, 'search'])->name('asesor.search');
});



Route::get('citas/index', [CitasController::class, 'index'])->name('citas.index');
Route::post('citas', [CitasController::class, 'store'])->name('citas.store');
Route::patch('citas/update/{id}', [CitasController::class, 'update'])->name('citas.update');
Route::patch('citas/updateDatos/{id}', [CitasController::class, 'updateDatos'])->name('citas.updateDatos');
Route::delete('citas/destroy/{id}', [CitasController::class, 'destroy'])->name('citas.destroy');


Route::group(['prefix' => 'email'], function () {
    Route::get('inbox', function () {
        return view('pages.email.inbox');
    });
    Route::get('read', function () {
        return view('pages.email.read');
    });
    Route::get('compose', function () {
        return view('pages.email.compose');
    });
});

Route::group(['prefix' => 'apps'], function () {
    Route::get('chat', function () {
        return view('pages.apps.chat');
    });
    Route::get('agendarCita', function () {
        return view('pages.apps.agendarCita');
    });
    Route::get('calendar', function () {
        return view('pages.apps.calendar');
    });
});

Route::group(['prefix' => 'cruds'], function () {
    Route::get('agendarCita', function () {
        return view('pages.cruds.agendarCita');
    });
    Route::get('buscarCita', function () {
        return view('pages.cruds.buscarCita');
    });
    Route::get('modificarCita', function () {
        return view('pages.cruds.modificarCita');
    });
    Route::get('sedesAdmin', function () {
        return view('pages.cruds.sedesAdmin');
    });
});



Route::group(['prefix' => 'ui-components'], function () {
    Route::get('accordion', function () {
        return view('pages.ui-components.accordion');
    });
    Route::get('alerts', function () {
        return view('pages.ui-components.alerts');
    });
    Route::get('badges', function () {
        return view('pages.ui-components.badges');
    });
    Route::get('breadcrumbs', function () {
        return view('pages.ui-components.breadcrumbs');
    });
    Route::get('buttons', function () {
        return view('pages.ui-components.buttons');
    });
    Route::get('button-group', function () {
        return view('pages.ui-components.button-group');
    });
    Route::get('cards', function () {
        return view('pages.ui-components.cards');
    });
    Route::get('carousel', function () {
        return view('pages.ui-components.carousel');
    });
    Route::get('collapse', function () {
        return view('pages.ui-components.collapse');
    });
    Route::get('dropdowns', function () {
        return view('pages.ui-components.dropdowns');
    });
    Route::get('list-group', function () {
        return view('pages.ui-components.list-group');
    });
    Route::get('media-object', function () {
        return view('pages.ui-components.media-object');
    });
    Route::get('modal', function () {
        return view('pages.ui-components.modal');
    });
    Route::get('navs', function () {
        return view('pages.ui-components.navs');
    });
    Route::get('navbar', function () {
        return view('pages.ui-components.navbar');
    });
    Route::get('pagination', function () {
        return view('pages.ui-components.pagination');
    });
    Route::get('popovers', function () {
        return view('pages.ui-components.popovers');
    });
    Route::get('progress', function () {
        return view('pages.ui-components.progress');
    });
    Route::get('scrollbar', function () {
        return view('pages.ui-components.scrollbar');
    });
    Route::get('scrollspy', function () {
        return view('pages.ui-components.scrollspy');
    });
    Route::get('spinners', function () {
        return view('pages.ui-components.spinners');
    });
    Route::get('tabs', function () {
        return view('pages.ui-components.tabs');
    });
    Route::get('tooltips', function () {
        return view('pages.ui-components.tooltips');
    });
});

Route::group(['prefix' => 'advanced-ui'], function () {
    Route::get('cropper', function () {
        return view('pages.advanced-ui.cropper');
    });
    Route::get('owl-carousel', function () {
        return view('pages.advanced-ui.owl-carousel');
    });
    Route::get('sweet-alert', function () {
        return view('pages.advanced-ui.sweet-alert');
    });
});

Route::group(['prefix' => 'forms'], function () {
    Route::get('basic-elements', function () {
        return view('pages.forms.basic-elements');
    });
    Route::get('advanced-elements', function () {
        return view('pages.forms.advanced-elements');
    });
    Route::get('editors', function () {
        return view('pages.forms.editors');
    });
    Route::get('wizard', function () {
        return view('pages.forms.wizard');
    });
});

Route::group(['prefix' => 'charts'], function () {
    Route::get('apex', function () {
        return view('pages.charts.apex');
    });
    Route::get('chartjs', function () {
        return view('pages.charts.chartjs');
    });
    Route::get('flot', function () {
        return view('pages.charts.flot');
    });
    Route::get('morrisjs', function () {
        return view('pages.charts.morrisjs');
    });
    Route::get('peity', function () {
        return view('pages.charts.peity');
    });
    Route::get('sparkline', function () {
        return view('pages.charts.sparkline');
    });
});

Route::group(['prefix' => 'tables'], function () {
    Route::get('basic-tables', function () {
        return view('pages.tables.basic-tables');
    });
    Route::get('data-table', function () {
        return view('pages.tables.data-table');
    });
});

Route::group(['prefix' => 'icons'], function () {
    Route::get('feather-icons', function () {
        return view('pages.icons.feather-icons');
    });
    Route::get('flag-icons', function () {
        return view('pages.icons.flag-icons');
    });
    Route::get('mdi-icons', function () {
        return view('pages.icons.mdi-icons');
    });
});

Route::group(['prefix' => 'general'], function () {
    Route::get('blank-page', function () {
        return view('pages.general.blank-page');
    });
    Route::get('faq', function () {
        return view('pages.general.faq');
    });
    Route::get('invoice', function () {
        return view('pages.general.invoice');
    });
    Route::get('profile', function () {
        return view('pages.general.profile');
    });
    Route::get('pricing', function () {
        return view('pages.general.pricing');
    });
    Route::get('timeline', function () {
        return view('pages.general.timeline');
    });
});

Route::group(['prefix' => 'auth'], function () {
    Route::get('login', function () {
        return view('pages.auth.login');
    });
    Route::get('register', function () {
        return view('pages.auth.register');
    });
    Route::get('forgotPass', function () {
        return view('pages.auth.forgotPass');
    });
    Route::get('newPass', function () {
        return view('pages.auth.newPass');
    });
});

Route::group(['prefix' => 'error'], function () {
    Route::get('404', function () {
        return view('pages.error.404');
    });
    Route::get('500', function () {
        return view('pages.error.500');
    });
});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

// 404 for undefined routes
Route::any('/{page?}', function () {
    return View::make('pages.error.404');
})->where('page', '.*');
