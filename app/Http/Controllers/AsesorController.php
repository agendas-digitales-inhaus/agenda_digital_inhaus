<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Citas;
use DateTime;
use Auth;
use Exception;
use Illuminate\Http\Request;

class AsesorController extends Controller
{

    function indexAsesor(){
        $events = array();
        $usuarioLogueado = auth()->user()->id;
                $citas = Citas::where('user_id', '=', $usuarioLogueado)->paginate();
            foreach($citas as $cita){
             $events[]= [
                 'id' => $cita->id,
                 'title' => $cita->nombreLead,
                 'telefono' => $cita->telefono,
                 'correo' => $cita->correo,
                 'idPropiedades' => $cita->idPropiedades,
                 'color' => $cita->color,
                 'start' => $cita->inicio,
                 'end' => $cita->fin,
                 'user_id' => $cita->user_id
             ];
            }
     
       
        return view('dashboards.asesor.dashboard', ['events'=> $events, 'prueba'=>$usuarioLogueado]);
    }

    public function indexBuscarCitas(Request $request)
    {

        
        if($request) {
            $query = trim($request->get(key:'search'));
            $usuarioLogueado = auth()->user()->id;
            $users =  Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->where('nombreLead', 'LIKE', '%' . $query . '%')->get();

        }
 
        $usuarioLogueado = auth()->user()->id;
        $citas = Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->paginate();
       

        return view('dashboards.asesor.buscarCita',  ['users'=> $users, 'search'=> $query]);
    }


    
    public function show($id)
    {
            return view('dashboards.asesor.editarCita', ['cita' => Citas::findOrFail($id)]);
    }

    public function update(Request $request,$id)
    {


        $citas = Citas::findOrFail($id);

        $citas->nombreLead = $request->get(key:'nombreLead');
        $citas->telefono = $request->get(key:'telefono');
        $citas->correo = $request->get(key:'correo');
        $citas->idPropiedades = $request->get(key:'idPropiedades');
        $citas->color = $request->get(key:'color');
        $citas->inicio = $request->get(key:'inicio');
        $citas->fin = $request->get(key:'fin');

        $citas->update();

        return view('dashboards.asesor.editarCita', ['cita' => Citas::findOrFail($id)]);
       /* return view('dashboards.asesor.buscarCita');*/

        /*
        $citas = Citas::findOrFail($id);
        if(! $citas){
            return response()->json([
                'error'=>'no se encontro la cita'
            ], 404);
        }
        $citas->update([
            'nombreLead' => $request->get(key:'nombreLead'),
                'telefono' => $request->get(key:'telefono'),
                'correo' => $request->get(key:'correo'),
                'idPropiedades' => $request->get(key:'idPropiedades'),
                'color' => $request->get(key:'color'),
                'inicio' => $request->get(key:'inicio'),
                'fin' => $request->get(key:'fin'),
        ]);
  */

            /*return view('dashboards.asesor.editarCita', ['cita' => Citas::findOrFail($id)]);*/
    }
    public function search(Request $request)
   {
    $usuarioLogueado = auth()->user()->id;
    $cita =  Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->where('nombreLead', 'like', '%' . $request->get('query') . '%')->get();
    return json_encode( $cita );
    }
}
