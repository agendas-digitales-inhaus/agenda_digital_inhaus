<?php

namespace App\Http\Controllers;

use App\Models\Citas;
use App\Http\Controllers\Controller;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = array();
        $citas = Citas::all();
       foreach($citas as $cita){
        $events[]= [
            'id' => $cita->id,
            'title' => $cita->nombreLead,
            'telefono' => $cita->telefono,
            'correo' => $cita->correo,
            'idPropiedades' => $cita->idPropiedades,
            'color' => $cita->color,
            'start' => $cita->inicio,
            'end' => $cita->fin,
            'user_id' => $cita->user_id
        ];
       }
        return view('citas.index', ['events'=> $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $prueba = auth()->user()->sede;
        $request->validate([

            'nombreLead' => 'required|string'

        ]);

            $citas = Citas::create([
                'nombreLead' => $request->nombreLead,
                'telefono' => $request->telefono,
                'correo' => $request->correo,
                'idPropiedades' => $request->idPropiedades,
                'color' => $request->color,
                'inicio' => $request->inicio,
                'fin' => $request->fin,
                'user_id' => Auth::id(),
                'sede' => Auth::user()->sede,
                'nombreAsesor' => Auth::user()->name
            ]);

            $fecha = new DateTime($request->inicio);

            $email_data = array(
                'name' => auth()->user()->name,
                'email' => auth()->user()->email,
                'nombreLead' => $request->nombreLead,
                'telefono' => $request->telefono,
                'correo' => $request->correo,
                'idPropiedades' => $request->idPropiedades,
                'inicio' => $fecha->format('d-m-Y H:i')
            );

            Mail::send('pages.email.ConfirmaCita', $email_data, function ($message) use ($email_data) {
                $message->to($email_data['email'], $email_data['name'])
                    ->subject('Recordatorio de cita');
            });
            
            /*
            Mail::send('pages.email.ConfirmarCita', $email_data, function ($message)  {
                $message->to('m.rodriguez@inhauscr.com', 'Mario')
                    ->subject('Has agandado una cita con: ');
            });
*/

    


        return response()->json($citas);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function show(Citas $citas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function edit(Citas $citas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $citas = Citas::find($id);
        if(! $citas){
            return response()->json([
                'error'=>'no se encontro la cita'
            ], 404);
        }
        $citas->update([
            'inicio' => $request->inicio,
            'fin' => $request->fin
        ]);
        
        return response()->json('Cita modificada');
    }

    public function updateDatos(Request $request, $id)
    {
        $citas = Citas::find($id);
        
        if( auth()->user()->id == $request->user_id){
            if(! $citas){
                return response()->json([
                    'error'=>'no se encontro la cita'
                ], 404);
            }
            $citas->update([
                'nombreLead' => $request->nombreLead,
                    'telefono' => $request->telefono,
                    'correo' => $request->correo,
                    'idPropiedades' => $request->idPropiedades,
                    'color' => $request->color
            ]);
            
            return response()->json('Cita');
        }else {
            return redirect()->back()->with('alert', 'no tiene permiso');
        }

        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Citas  $citas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $citas = Citas::find($id);
        if(auth()->user()->id == $request->user_id){
            if(! $citas){
                return response()->json([
                    'error'=>'no se encontro la cita'
                ], 404);
            }
            $citas->delete();
            return $id;
        }else {
            return redirect()->back()->with('alert', 'no tiene permiso');
        }
    }
}
