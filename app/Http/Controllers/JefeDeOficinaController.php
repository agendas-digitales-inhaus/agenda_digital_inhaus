<?php

namespace App\Http\Controllers;
use App\Models\Citas;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class JefeDeOficinaController extends Controller
{
    function index(){
        $events = array();
        $usuarioLogueado = Auth::user()->sede;
        $usuarioSede = Auth::user()->id;
        $filtroAsesores = User::where('role', '=', '2')->where('sede', '=', $usuarioLogueado)->get();
        $citas = Citas::where('sede', '=', $usuarioLogueado)->when(request(key:'user_id'),function($query){
            $usuarioLogueado = auth()->user()->sede;
            return $query->where('user_id', request(key: 'user_id'));
        })->get();

       foreach($citas as $cita){
        $events[]= [
            'id' => $cita->id,
            'title' => $cita->nombreLead,
            'telefono' => $cita->telefono,
            'correo' => $cita->correo,
            'idPropiedades' => $cita->idPropiedades,
            'color' => $cita->color,
            'start' => $cita->inicio,
            'end' => $cita->fin,
            'user_id' => $cita->user_id
        ];
       }
       return view('dashboards.jefeOficina.dashboard', ['events'=> $events, 'filtro'=>$filtroAsesores, 'usuarioLogueado'=>$usuarioLogueado, 'citas'=>$citas]);
    }


    public function indexBuscarCitas(Request $request)
    {

        
        if($request) {
            $query = trim($request->get(key:'search'));
            $usuarioLogueado = auth()->user()->id;
            $users =  Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->where('nombreLead', 'LIKE', '%' . $query . '%')->get();

        }
 
        $usuarioLogueado = auth()->user()->id;
        $citas = Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->paginate();
    
       

        return view('dashboards.jefeOficina.buscarCita',  ['users'=> $users, 'search'=> $query]);
    }

    public function Sede(Request $request)
    {
        $sede = Auth::user()->sede;
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', $sede)->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', $sede)->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', $sede)->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', $sede)->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', $sede)->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', $sede)->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', $sede)->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', $sede)->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', $sede)->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', $sede)->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', $sede)->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', $sede)->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', $sede)
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.JefeOficina.Sede', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function search(Request $request)
    {
     $usuarioLogueado = auth()->user()->id;
     $cita =  Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->where('nombreLead', 'like', '%' . $request->get('query') . '%')->get();
     return json_encode( $cita );
     }
    
}
