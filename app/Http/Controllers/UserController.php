<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;





class UserController extends Controller
{
    public function index(Request $request)
    {
        $prueba = User::where('role', '=', '2')->paginate();
        $jefe = User::where('role', '=', '3')->paginate();
        $admin = User::where('role', '=', '1')->paginate();


        if($request) {
            $query = trim($request->get(key:'search'));
            $usuarioLogueado = auth()->user()->id;
            $usuarios =  User::where('name', 'LIKE', '%' . $query . '%')->get();

        }
       // $usuarios = User::orderBy('name', 'desc')->paginate();
        return view('pages.cruds.users', compact('usuarios', 'prueba', 'jefe', 'admin'));
    }


    //Cambia a request para poder autogenerar
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required|max:255',
            'email' => 'required|email|unique:users',
            'role' => 'required',
            'sede' => 'required',
            'apellido1' => 'required',
            'apellido2' => 'required',
            'cedula' => 'required'
        ]);

        $pw = bcrypt(Str::random(15));

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password =  Hash::make($pw);
        $user->role = $request->role;
        $user->sede = $request->sede;
        $user->apellido1 = $request->apellido1;
        $user->apellido2 = $request->apellido2;
        $user->cedula = $request->cedula;

        $user->save();

        /* $user = User::create($request->all()); */

        $email_data = array(
            'name' => $request->name,
            'email' => $request->email,
            'password' => $pw

        );

        Mail::send('pages.email.CreatePassmail', $email_data, function ($message) use ($email_data) {
            $message->to($email_data['email'], $email_data['name'])
                ->subject('Bienvenido al Sistema Agendas Digitales InHaus');
        });


        return redirect()->route('users', $user);
    }

    public function show(User $user)
    {
        return view('pages.cruds.userEdit', compact('user'));
    }

    public function update(User $user, Request $request)
    {

        $user->update($request->all());
        return redirect()->route('users');
    }

    public function destroy(User $user)
    {

        $user->delete();

        return redirect()->route('users');
    }
    public function search(Request $request)
   {
    $usuario =  User::where('name', 'like', '%' . $request->get('query') . '%')->get();
    return json_encode( $usuario );
    }
}
