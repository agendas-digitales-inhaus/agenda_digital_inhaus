<?php

namespace App\Http\Controllers;
use App\Models\Citas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class AdminController extends Controller
{
    function index(){
        $events = array();
        $usuarioLogueado = auth()->user()->id;
        $filtroAsesores = User::where('role', '=', '2')->paginate();
        $citas = Citas::when(request(key:'user_id'),function($query){
            return $query->where('user_id', request(key: 'user_id'));
        })->get();
       foreach($citas as $cita){
        $events[]= [
            'id' => $cita->id,
            'title' => $cita->nombreLead,
            'telefono' => $cita->telefono,
            'correo' => $cita->correo,
            'idPropiedades' => $cita->idPropiedades,
            'color' => $cita->color,
            'start' => $cita->inicio,
            'end' => $cita->fin,
            'user_id' => $cita->user_id
        ];
       }
       return view('dashboards.admin.dashboard', ['events'=> $events, 'filtro'=>$filtroAsesores]);
    }
    public function indexBuscarCitas(Request $request)
    {

        
        if($request) {
            $query = trim($request->get(key:'search'));
            $usuarioLogueado = auth()->user()->id;
            $users =  Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->where('nombreLead', 'LIKE', '%' . $query . '%')->get();
            
        }
 
        $usuarioLogueado = auth()->user()->id;
        
        $citas = Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->paginate();
    
       if($citas=null){
          $citas = "no hay datos";
       }

        return view('dashboards.admin.buscarCita',  ['users'=> $users, 'search'=> $query]);
    }

    public function SanJoseEste(Request $request)
    {
       
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', 'San José Este')->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', 'San José Este')->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', 'San José Este')->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', 'San José Este')->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', 'San José Este')->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', 'San José Este')->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', 'San José Este')->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', 'San José Este')->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', 'San José Este')->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', 'San José Este')->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', 'San José Este')->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', 'San José Este')->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', 'San José Este')
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.admin.SanJoseEste', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function SanJoseNorte(Request $request)
    {
       
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', 'San José Norte')->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', 'San José Norte')->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', 'San José Norte')->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', 'San José Norte')->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', 'San José Norte')->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', 'San José Norte')->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', 'San José Norte')->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', 'San José Norte')->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', 'San José Norte')->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', 'San José Norte')->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', 'San José Norte')->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', 'San José Norte')->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', 'San José Norte')
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.admin.SanJoseNorte', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function SanJoseSur(Request $request)
    {
       
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', 'San José Sur')->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', 'San José Sur')->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', 'San José Sur')->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', 'San José Sur')->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', 'San José Sur')->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', 'San José Sur')->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', 'San José Sur')->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', 'San José Sur')->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', 'San José Sur')->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', 'San José Sur')->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', 'San José Sur')->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', 'San José Sur')->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', 'San José Sur')
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.admin.SanJoseSur', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function SanJoseOeste(Request $request)
    {
       
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', 'San José Oeste')->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', 'San José Oeste')->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', 'San José Oeste')->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', 'San José Oeste')->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', 'San José Oeste')->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', 'San José Oeste')->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', 'San José Oeste')->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', 'San José Oeste')->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', 'San José Oeste')->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', 'San José Oeste')->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', 'San José Oeste')->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', 'San José Oeste')->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', 'San José Oeste')
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.admin.SanJoseOeste', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function Heredia(Request $request)
    {
       
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', 'Heredia')->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', 'Heredia')->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', 'Heredia')->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', 'Heredia')->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', 'Heredia')->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', 'Heredia')->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', 'Heredia')->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', 'Heredia')->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', 'Heredia')->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', 'Heredia')->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', 'Heredia')->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', 'Heredia')->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', 'Heredia')
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.admin.Heredia', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function Comercial(Request $request)
    {
       
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', 'Comercial')->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', 'Comercial')->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', 'Comercial')->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', 'Comercial')->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', 'Comercial')->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', 'Comercial')->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', 'Comercial')->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', 'Comercial')->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', 'Comercial')->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', 'Comercial')->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', 'Comercial')->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', 'Comercial')->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', 'Comercial')
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.admin.Comercial', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function Cartago(Request $request)
    {
       
        $enero = Citas::whereMonth('inicio', '=', '01')->where('sede', '=', 'Cartago')->paginate();
        $febrero = Citas::whereMonth('inicio', '=', '02')->where('sede', '=', 'Cartago')->paginate();
        $marzo = Citas::whereMonth('inicio', '=', '03')->where('sede', '=', 'Cartago')->paginate();
        $abril = Citas::whereMonth('inicio', '=', '04')->where('sede', '=', 'Cartago')->paginate();
        $mayo = Citas::whereMonth('inicio', '=', '05')->where('sede', '=', 'Cartago')->paginate();
        $junio = Citas::whereMonth('inicio', '=', '06')->where('sede', '=', 'Cartago')->paginate();
        $julio = Citas::whereMonth('inicio', '=', '07')->where('sede', '=', 'Cartago')->paginate();
        $agosto = Citas::whereMonth('inicio', '=', '08')->where('sede', '=', 'Cartago')->paginate();
        $septiembre = Citas::whereMonth('inicio', '=', '09')->where('sede', '=', 'Cartago')->paginate();
        $octubre = Citas::whereMonth('inicio', '=', '10')->where('sede', '=', 'Cartago')->paginate();
        $noviembre = Citas::whereMonth('inicio', '=', '11')->where('sede', '=', 'Cartago')->paginate();
        $diciembre = Citas::whereMonth('inicio', '=', '12')->where('sede', '=', 'Cartago')->paginate();

        $donutChart = DB::table('citas')
        ->selectRaw('count(*) as total_cita, nombreAsesor')
        ->where('sede', '=', 'Cartago')
        ->whereMonth('inicio', now()->month)
        ->groupBy('nombreAsesor')
        ->get();
        
        /*DB::select(DB::raw("select count(*) as total_cita, nombreAsesor from citas where sede='sanjose' group by nombreAsesor"));*/

        $Data="";
        foreach($donutChart as $Chart){
            $Data.="['".$Chart->nombreAsesor."',           ".$Chart->total_cita."],";
        }
        $arr['Data']=rtrim($Data,",");
        return view('dashboards.admin.Cartago', $arr, compact('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'
        , 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'));
    }
    public function search(Request $request)
    {
     $usuarioLogueado = auth()->user()->id;
     $cita =  Citas::whereMonth('inicio', now()->month)->where('user_id', '=', $usuarioLogueado)->where('nombreLead', 'like', '%' . $request->get('query') . '%')->get();
     return json_encode( $cita );
     }
    
}
