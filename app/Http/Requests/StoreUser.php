<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'=> 'required',
            'email' => 'required',
            'role' => 'required',
            'sede' => 'required',
            'apellido1' => 'required',
            'apellido2' => 'required',
            'cedula' => 'required',
           /*  'password' => 'required' */


        ];
    }

    public function messages()
    {
        return[
            'email' => 'El espacio email debe ser ingresado'
        ];
    }
}
