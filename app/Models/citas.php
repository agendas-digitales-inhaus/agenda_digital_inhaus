<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{
    use HasFactory;

    protected $fillable = ['nombreLead','telefono','correo','idPropiedades','color','inicio','fin','user_id','sede','nombreAsesor'];
}
